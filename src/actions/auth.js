import { firebase, database, provider } from "../firebase/firebase";

export const setAuthInfo = ( { isAuthorized, displayName, email, token } ) => ({
    type : "SET_AUTH",
    isAuthorized,
    displayName,
    email,
    token    
});

export const checkAuth = () => {
    
    return ( dispatch ) => {

        firebase.auth().signInWithPopup( provider).then( ( result ) => {

            // This gives you a Google Access Token. You can use it to access the Google API.
            const acccessToken = result.credential.accessToken;
            // The signed-in user info.
            const user = result.user;
            const displayName = user.displayName;
            const email = user.email;
            const token = user.uid;

            dispatch( setAuthInfo({
                isAuthorized : true,
                displayName,
                email,
                token
            }));
            
            console.log( token );
            // ...
        }).catch( ( error ) => {
            // Handle Errors here.
            const errorCode = error.code;
            const errorMessage = error.message;
            // The email of the user's account used.
            const email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            const credential = error.credential;
            // ...
            dispatch( setAuthInfo({
                isAuthorized : false,
                email,
                displayName : "",
                token : ""
            }));
            
        });

    }

};
