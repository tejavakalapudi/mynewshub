import axios from "axios";
import { firebase, database, provider } from "../firebase/firebase";

export const setArticles = ( articles = [] ) => ({
    type : "SET_ARTICLES",
    articles    
});

export const setLiked = ( likedArticles = [] ) => ({
    type : "SET_LIKED_ARTICLES",
    likedArticles    
});

export const setLikedArticles = ( likedArticles = [], userToken ) => {
    
    return( dispatch ) => {

        database.ref( `articles/${userToken}` ).set( likedArticles );

        dispatch( setLiked( likedArticles ) );
        
    }

};

export const getLikedArticles = ( token ) => {

    return( dispatch ) => {
    
        database.ref( `articles/${token}` ).once( "value" )

        .then(( snapshot ) => {

                const articles = snapshot.val() || []

                dispatch( setLiked( articles ) );
        
            }
        )
        .catch( ( e ) => {

            console.log( "Fetching liked articles data has failed with error ", e );
            
        });

    }

}


export const startSetArticles = () => {
    
    return( dispatch ) => {

        var event = new Date();

        console.log( "Requesting articles from api at ", event.toTimeString());

        return axios({

            method:"get",
            url:"https://api.myjson.com/bins/10ijyt"

        })
        .then(( res ) => {

            dispatch( setArticles( res.data ) );

        })
        .catch(( e ) => {
    
            console.log( "Fetching articles has failed with an error ", e );

        })

    }

};