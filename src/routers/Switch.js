//https://reacttraining.com/react-router/web/guides/philosophy

import React from "react";
import { Route, Switch, withRouter } from "react-router-dom"; 

import Header from "../components/Header";
import HomePage from "../components/HomePage";
import NotFound from "../components/NotFoundPage";
import LoginScreen from "../components/LoginPage";
import PrivateRoute from "./PrivateRoute";

class SwitchComponent extends React.Component {

    render(){

        return(
            <div>
                <Switch>
                    <Route path = "/" component = { LoginScreen } exact={true}/>
                    <PrivateRoute path = "/home" component = { HomePage }/>
                    <PrivateRoute path = "/liked" component = { HomePage }/>
                    <Route component = { NotFound } />
                </Switch>
            </div>
        );
    }
}

export default withRouter( SwitchComponent );
