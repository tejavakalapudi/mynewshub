import * as firebase from "firebase";

  // Initialize Firebase
const config = {
    apiKey: "AIzaSyArQmAjW_wM3bSRFHl0PakCx37rlv_GjDo",
    authDomain: "mynewshub-83df9.firebaseapp.com",
    databaseURL: "https://mynewshub-83df9.firebaseio.com",
    projectId: "mynewshub-83df9",
    storageBucket: "mynewshub-83df9.appspot.com",
    messagingSenderId: "876714403314"
};

firebase.initializeApp( config );

const provider = new firebase.auth.GoogleAuthProvider();
const database = firebase.database();

export { firebase, database, provider };