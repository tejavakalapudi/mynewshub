const authDefault = {
    isAuthorized : false,
    displayName : "",
    email : "",
    message : "Something went wrong!",
    token : ""
};

export default ( state = authDefault, action ) => {

    switch( action.type ){

        case "SET_AUTH" : {
            return { ...state, isAuthorized : action.isAuthorized, displayName : action.displayName, email : action.email, token : action.token };
        }

        default:
        return state;
        
    }      
};