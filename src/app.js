import React from "react";
import ReactDOM from "react-dom";
import configureStore from "./store/configureStore";
import { startSetArticles } from "./actions/articles";
import { Provider } from "react-redux";
import NewsApp from "./components/Template";
import LoadingScreen from "./components/LoadingPage";
import { checkAuth } from "./actions/auth";
import LoginScreen from "./components/LoginPage";

import "normalize.css/normalize.css";
import "./styles/styles.scss";

const store = configureStore();

store.dispatch( startSetArticles() );

ReactDOM.render( <NewsApp store = { store } />, document.getElementById( "app" ) );


