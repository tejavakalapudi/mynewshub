import React from "react";
import { Container, Row, Col, Button } from "reactstrap";
import { checkAuth } from "../actions/auth";
import { getLikedArticles } from "../actions/articles";
import { connect } from "react-redux";

class LoginPage extends React.Component {

    checkAuth = () => {

        this.props.dispatch( checkAuth() );
        
    }

    componentDidUpdate( prevProps ) {

        if ( this.props.auth.isAuthorized !== prevProps.auth.isAuthorized && this.props.auth.isAuthorized ) {

            this.props.history.push( "./home" );

            this.props.dispatch( getLikedArticles( this.props.auth.token ) );
            
        }

    }

    render(){
        return(
            <Container>
        
                <Row className="justify-content-center" >
                    <Col xs="12" className="text__align-center"> Please login using Gmail credentials</Col>
                    <Button onClick = { this.checkAuth } >Redirect</Button>
                </Row>
        
            </Container>
        )
    }

}

const mapStateToProps = ( store ) => {

    return {
        auth : store.auth
    }

}

export default connect( mapStateToProps )( LoginPage );